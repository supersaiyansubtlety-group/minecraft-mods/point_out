package net.sssubtlety.point_out;

import com.mojang.blaze3d.platform.InputUtil;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBind;
import net.minecraft.util.ActionResult;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.glfw.GLFW;

import java.awt.*;

import static net.sssubtlety.point_out.PointOut.NAMESPACE;
import static net.sssubtlety.point_out.Util.isModLoaded;

public class FeatureControl {
    private static final @Nullable Config CONFIG_INSTANCE;

    public static final KeyBind START_POINT_SELECTION = registerKeyBind("start_point_selection", GLFW.GLFW_KEY_O);
    public static final KeyBind SHOW_SELECTION = registerKeyBind("show_selection", GLFW.GLFW_KEY_I);
    public static final KeyBind MOVE_SELECTION_CLOSER = registerKeyBind("move_selection_closer", GLFW.GLFW_KEY_PAGE_DOWN);
    public static final KeyBind MOVE_SELECTION_FARTHER = registerKeyBind("move_selection_farther", GLFW.GLFW_KEY_PAGE_UP);
    public static final KeyBind SEND_SELECTION_TO_NEARBY = registerKeyBind("send_selection_to_nearby", GLFW.GLFW_KEY_P);
    public static final KeyBind SEND_SELECTION_TO_INDIVIDUAL = registerKeyBind("send_selection_to_individual", GLFW.GLFW_KEY_UNKNOWN);
    public static final KeyBind SEND_SELECTION_TO_EVERYONE = registerKeyBind("send_selection_to_everyone", GLFW.GLFW_KEY_UNKNOWN);
    public static final KeyBind CLEAR_POINTS = registerKeyBind("clear_points", GLFW.GLFW_KEY_UNKNOWN);

    private static float[] pointColorComponents;
    private static float[] selectionColorComponents;

    static {
        if (isModLoaded("cloth-config", ">=6.1.48")) {
            final ConfigHolder<Config> configHolder = AutoConfig.register(Config.class, GsonConfigSerializer::new);
            CONFIG_INSTANCE = configHolder.getConfig();
            configHolder.registerLoadListener(FeatureControl::onConfigChange);
            configHolder.registerSaveListener(FeatureControl::onConfigChange);
            onConfigChange(configHolder, CONFIG_INSTANCE);
        } else {
            CONFIG_INSTANCE = null;
            pointColorComponents = Defaults.point_outline_color.getRGBColorComponents(null);
            selectionColorComponents = Defaults.selection_outline_color.getRGBColorComponents(null);
        }
    }

    private static ActionResult onConfigChange(ConfigHolder<Config> holder, Config config) {
        pointColorComponents = new Color(CONFIG_INSTANCE.point_outline_color).getRGBColorComponents(null);
        selectionColorComponents = new Color(CONFIG_INSTANCE.selection_outline_color).getRGBColorComponents(null);
        return ActionResult.CONSUME;
    }

    public interface Defaults {
        int search_distance = 50;
        int point_duration = 200;
        int post_move_preview_duration = 20;
        int large_movement_increment = 10;
        Color point_outline_color = Color.white;
        Color selection_outline_color = Color.yellow;
        boolean fetch_translation_updates = true;
    }

    public static int getPointSearchDistance() {
        return CONFIG_INSTANCE == null ? Defaults.search_distance : CONFIG_INSTANCE.search_distance;
    }

    public static int getPointDuration() {
        return CONFIG_INSTANCE == null ? Defaults.point_duration : CONFIG_INSTANCE.point_duration;
    }

    public static int getPostMovePreviewDuration() {
        return CONFIG_INSTANCE == null ? Defaults.post_move_preview_duration : CONFIG_INSTANCE.post_move_preview_duration;
    }

    public static int getMovementIncrement() {
        return MinecraftClient.getInstance().player.isSneaking() ?
                (
                        CONFIG_INSTANCE == null ?
                                Defaults.large_movement_increment :
                                CONFIG_INSTANCE.large_movement_increment
                ) : 1;
    }

    public static float[] getPointColor() {
        return pointColorComponents;
    }

    public static float[] getSelectionColor() {
        return selectionColorComponents;
    }

    public static boolean shouldFetchTranslationUpdates() {
        return CONFIG_INSTANCE == null ? Defaults.fetch_translation_updates : CONFIG_INSTANCE.fetch_translation_updates;
    }

    public static boolean isConfigLoaded() {
        return CONFIG_INSTANCE != null;
    }

    public static void init() { }

    private static KeyBind registerKeyBind(String name, int glfwKey) {
        return KeyBindingHelper.registerKeyBinding(new KeyBind(
                "key." + NAMESPACE + "." + name,
                InputUtil.Type.KEYSYM,
                glfwKey,
                "category." + NAMESPACE
        ));
    }

    private FeatureControl() { }
}
