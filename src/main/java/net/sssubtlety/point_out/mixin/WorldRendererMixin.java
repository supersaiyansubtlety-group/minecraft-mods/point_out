package net.sssubtlety.point_out.mixin;

import net.minecraft.client.render.WorldRenderer;
import net.sssubtlety.point_out.PointOut;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

@Mixin(WorldRenderer.class)
public class WorldRendererMixin {
    @ModifyVariable(method = "render", ordinal = 3, at = @At(value = "INVOKE", target = "Lnet/minecraft/client/render/TexturedRenderLayers;getChest()Lnet/minecraft/client/render/RenderLayer;"))
    private boolean point_out$preEntityOutlineShaderCheck(boolean original) {
        return original || PointOut.isRenderingPoint();
    }
}
