package net.sssubtlety.point_out;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Formatting;
import net.minecraft.util.Language;

import java.util.Optional;

public final class Util {
    @SuppressWarnings("ConstantConditions")
    public static int getRgb(Text text) {
        final TextColor color = text.getStyle().getColor();
        if (color == null) return Formatting.WHITE.getColorValue();
        else return color.getRgb();
    }

    public static Text fillPlaceholder(Text text, String placeholder, String value) {
//        String string = text.getString();
//        string = string.replaceAll(placeholder, value);
        String string = fillPlaceholder(text.getString(), placeholder, value);
        return new LiteralText(string).setStyle(text.getStyle());
    }

    public static String fillPlaceholder(String string, String placeholder, String value) {
        return string.replaceAll("\\$\\{" + placeholder + "\\}", value);
    }

    public static boolean isModLoaded(String id, String versionPredicate) {
        final Optional<ModContainer> optModContainer = FabricLoader.getInstance().getModContainer(id);
        if (optModContainer.isPresent()){
            try {
                return VersionPredicate.parse(versionPredicate).test(optModContainer.get().getMetadata().getVersion());
            } catch (VersionParsingException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private Util() { }
}
