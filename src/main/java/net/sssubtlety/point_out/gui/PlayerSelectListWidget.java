package net.sssubtlety.point_out.gui;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.screen.narration.NarrationMessageBuilder;
import net.minecraft.client.gui.widget.EntryListWidget;
import net.minecraft.client.network.PlayerListEntry;
import net.minecraft.client.util.math.MatrixStack;

public class PlayerSelectListWidget extends EntryListWidget<PlayerSelectListWidget.PlayerEntry> {
    private final TextRenderer textRenderer;
    public PlayerSelectListWidget(MinecraftClient minecraftClient, int width, int height, int top, int bottom, int itemHeight, TextRenderer textRenderer) {
        super(minecraftClient, width, height, top, bottom, itemHeight);
        this.textRenderer = textRenderer;
        for (PlayerListEntry playerListEntry : this.client.getNetworkHandler().getPlayerList()) {
            PlayerEntry playerEntry = new PlayerEntry(playerListEntry);
            this.addEntry(playerEntry);
        }
        if (this.getSelectedOrNull() != null) {
            this.centerScrollOn(this.getSelectedOrNull());
        }
    }

    @Override
    public void appendNarrations(NarrationMessageBuilder builder) {

    }

    public class PlayerEntry extends EntryListWidget.Entry<PlayerEntry> {
        private final String playerName;
        public PlayerEntry(PlayerListEntry playerListEntry) {
            this.playerName = playerListEntry.getProfile().getName();
        }

        @Override
        public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            PlayerSelectListWidget.this.textRenderer.drawWithShadow(matrices, playerName, PlayerSelectListWidget.this.width / 2 - PlayerSelectListWidget.this.textRenderer.getWidth(playerName) / 2, y + 1, 0xFFFFFF, true);
        }
    }
}
