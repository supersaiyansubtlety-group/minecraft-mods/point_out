package net.sssubtlety.point_out.gui;

import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.widget.*;
import io.github.cottonmc.cotton.gui.widget.data.Insets;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.Window;
import net.minecraft.text.LiteralText;
import net.sssubtlety.point_out.PointOut;

import java.util.List;

public class PlayerSelectGui extends LightweightGuiDescription {
    public static final int GRID = 18;
    public static final int BUTTON_HEIGHT = 20;
    public static final int PADDED_BUTTON_HEIGHT = 24;

    private static void configureButton(String string, SendToPlayerButton button) {
        button.setLabel(new LiteralText(string));
    }



    public PlayerSelectGui() {
        WGridPanel root = new WGridPanel();
        setRootPanel(root);

        final List<String> playerNames =  MinecraftClient.getInstance().getNetworkHandler().getPlayerList().stream().map(playerListEntry -> playerListEntry.getProfile().getName()).toList();

        final Window window = MinecraftClient.getInstance().getWindow();
        int rootHeight = Math.min(window.getScaledHeight() / 3, playerNames.size() * PADDED_BUTTON_HEIGHT);
        root.setSize(window.getScaledWidth() / 3, rootHeight);
        root.setInsets(Insets.ROOT_PANEL);

        WListPanel<String, SendToPlayerButton> listPanel = new WListPanel<>(playerNames, SendToPlayerButton::new, PlayerSelectGui::configureButton);
        listPanel.setListItemHeight(BUTTON_HEIGHT);
        root.add(listPanel, 0, 1, root.getWidth() / GRID, root.getHeight() / GRID + 1);

        root.validate(this);
    }

    private static class SendToPlayerButton extends WButton {
        public SendToPlayerButton() {
            this.setOnClick(this::onClick);
        }

        private void onClick() {
            PointOut.sendSelectionToPlayer(this.getLabel().getString());
            MinecraftClient.getInstance().setScreen(null);
        }
    }

//    public static class SelectableEntryText extends WText {
//        private boolean selected = false;
//        public SelectableEntryText() {
//            super(placeholderText);
//        }
//
//        public void select() {
//            selected = true;
//        }
//
//        public void deselect() {
//            selected = false;
//        }
//
//        @Override
//        public void paint(MatrixStack matrices, int x, int y, int mouseX, int mouseY) {
//
//            super.paint(matrices, x, y, mouseX, mouseY);
//        }
//    }
}
