package net.sssubtlety.point_out;

import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class Selection {
    public final Identifier dimId;
    private Vec3d pos;
    private BlockPos blockPos;

    public Selection(Identifier dimId, Vec3d pos) {
        this.dimId = dimId;
        this.pos = pos;
        this.blockPos = new BlockPos(pos);
    }

    public void add(Vec3d vec) {
        this.pos = this.pos.add(vec);
        BlockPos newBlockPos = new BlockPos(this.pos);
        // add again if blockPos hasn't changed
        if (newBlockPos.equals(this.blockPos)) {
            this.pos = this.pos.add(vec);
            newBlockPos = new BlockPos(this.pos);
        }
        this.blockPos = newBlockPos;
    }

    public Vec3d getPos() {
        return this.pos;
    }

    public BlockPos getBlockPos() {
        return this.blockPos;
    }
}
