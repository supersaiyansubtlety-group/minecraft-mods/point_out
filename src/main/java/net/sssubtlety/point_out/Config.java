package net.sssubtlety.point_out;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import java.awt.*;

import static net.sssubtlety.point_out.PointOut.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class Config implements ConfigData {
    @ConfigEntry.BoundedDiscrete(min = 1, max = 1000)
    @ConfigEntry.Gui.Tooltip()
    int search_distance = FeatureControl.Defaults.search_distance;

    @ConfigEntry.BoundedDiscrete(min = 10, max = 12000)
    @ConfigEntry.Gui.Tooltip()
    int point_duration = FeatureControl.Defaults.point_duration;

    @ConfigEntry.BoundedDiscrete(min = 0, max = 12000)
    @ConfigEntry.Gui.Tooltip()
    int post_move_preview_duration = FeatureControl.Defaults.post_move_preview_duration;

    @ConfigEntry.Gui.Tooltip()
    @ConfigEntry.BoundedDiscrete(min = 1, max = 100)
    int large_movement_increment = FeatureControl.Defaults.large_movement_increment;

    @ConfigEntry.Gui.Tooltip()
    @ConfigEntry.ColorPicker
    int point_outline_color = FeatureControl.Defaults.point_outline_color.getRGB() & 0x00ffffff;

    @ConfigEntry.Gui.Tooltip()
    @ConfigEntry.ColorPicker
    int selection_outline_color = FeatureControl.Defaults.selection_outline_color.getRGB() & 0x00ffffff;

    @ConfigEntry.Gui.Tooltip()
    boolean fetch_translation_updates = FeatureControl.Defaults.fetch_translation_updates;
}
