package net.sssubtlety.point_out;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.rendering.v1.WorldRenderEvents;

import static net.sssubtlety.point_out.FeatureControl.shouldFetchTranslationUpdates;

public class ClientInit implements ClientModInitializer {

    static {
        FeatureControl.init();

        if (shouldFetchTranslationUpdates())
            CrowdinTranslate.downloadTranslations("point-out", PointOut.NAMESPACE);

        ClientTickEvents.START_WORLD_TICK.register(PointOut::onClientWorldTick);
//        WorldRenderEvents.BEFORE_BLOCK_OUTLINE.register(PointOut::render);
        WorldRenderEvents.BEFORE_ENTITIES.register(PointOut::render);
        WorldRenderEvents.END.register(PointOut::postRender);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void onInitializeClient() { }
}
