package net.sssubtlety.point_out;

import com.mojang.logging.LogUtils;
import net.fabricmc.fabric.api.client.rendering.v1.WorldRenderContext;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.*;
import net.minecraft.util.shape.VoxelShapes;
import net.sssubtlety.point_out.gui.PlayerSelectGui;
import net.sssubtlety.point_out.gui.PlayerSelectScreen;
import net.sssubtlety.point_out.mixin.WorldRendererAccessor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.sssubtlety.point_out.FeatureControl.*;
import static net.sssubtlety.point_out.Util.fillPlaceholder;

// TODO:
//  Confirm outlines render through blocks
//  Add command link in point messages to re-show point
//    - with optional dependency on client commands to show without whispering to self
//    - with separate (default longer) duration
//  Render edge-of-screen indicators if player is looking away from a point
//  Allow custom or random outline colors
//  Render distance from player at point
//  Render sender name at point

public class PointOut {
	public static final String NAMESPACE = "point_out";
	public static final Text NAME = new TranslatableText("text." + NAMESPACE + ".name");
	public static final Logger LOGGER = LogUtils.getLogger();
	public static final String INT_REGEX_GROUP = "(-?\\d+)";
	public static final String DIMENSION_REGEX_GROUP = "(\\w+:\\w+)";

	public static final Text CREATED_POINT = new TranslatableText("chat.point_out.created_point");

	public static final Text NO_SELECTION = new TranslatableText("message.point_out.no_selection");
	public static final Text SELECTION_NOT_IN_DIMENSION = new TranslatableText("message.point_out.selection_not_in_dimension");
	public static final Text SELECTION_NOT_LOADED = new TranslatableText("message.point_out.selection_too_far");
	public static final Text PLAYER_SELECT_TITLE = new TranslatableText("screen.point_out.player_select.title");
	public static final Text DISTANCE = new TranslatableText("text.point_out.distance");

	public static final Pattern CREATE_POINT_PATTERN = Pattern.compile(getCreatePointMessageTail(INT_REGEX_GROUP, INT_REGEX_GROUP, INT_REGEX_GROUP, DIMENSION_REGEX_GROUP) + "$");

	private static @Nullable Selection selection;
	private static int selectionDisplayLife = 0;
	private static boolean selectionUpdated = false;
	private static final Map<Pair<Identifier, BlockPos>, Integer> points = new LinkedHashMap<>();
	private static boolean renderingPoint = false;

	@NotNull
	private static String getCreatePointMessageTail(String x, String y, String z, String dimension) {
		return " " + NAMESPACE + ": " + x + ", " + y + ", " + z + "; dimension: " + dimension;
	}

	public static boolean isRenderingPoint() {
		return renderingPoint;
	}

	public static void onClientWorldTick(ClientWorld world) {
		if (CLEAR_POINTS.wasPressed()) {
			points.clear();
		}

		if (selectionDisplayLife > 0) selectionDisplayLife--;

		if (MOVE_SELECTION_CLOSER.wasPressed()) moveSelection(true);
		else if (MOVE_SELECTION_FARTHER.wasPressed()) moveSelection(false);

		if (!points.isEmpty()) {
			Iterator<Map.Entry<Pair<Identifier, BlockPos>, Integer>> pointsItr = points.entrySet().iterator();
			Map.Entry<Pair<Identifier, BlockPos>, Integer> entry;
			while (pointsItr.hasNext()) {
				entry = pointsItr.next();
				// remove expired entries
				if (entry.getValue() == 0) pointsItr.remove();
				// decrement entries with remaining time
				else entry.setValue(entry.getValue() - 1);
			}
		}

		selectionUpdated = false;
	}

	public static void onChat(String message) {
		Matcher matcher = CREATE_POINT_PATTERN.matcher(message);
		if (matcher.find()) {
			final String xString = matcher.group(1);
			if (xString == null) return;
			final String yString = matcher.group(2);
			if (yString == null) return;
			final String zString = matcher.group(3);
			if (zString == null) return;
			final String dimString = matcher.group(4);
			if (dimString == null) return;

			try {
				Identifier dimId = new Identifier(dimString);
				int x = Integer.parseInt(xString);
				int y = Integer.parseInt(yString);
				int z = Integer.parseInt(zString);

				points.put(new Pair<>(dimId, new BlockPos(x, y, z)), getPointDuration());
			} catch (NumberFormatException ignored) { }
		}
	}

	private static Identifier getCurrentDimId() {
		return MinecraftClient.getInstance().world.getDimension().getSkyProperties();
	}

	public static void render(WorldRenderContext context) {
		final Identifier currentDimId = getCurrentDimId();
		points.keySet().forEach(dimIdAndPoint -> {
			// only render points in this dimension
			if (currentDimId.equals(dimIdAndPoint.getLeft())) {
				renderingPoint = true;
				float[] colorComponents = getPointColor();
				outlinePos(dimIdAndPoint.getRight(), context, colorComponents[0], colorComponents[1], colorComponents[2], 1f);
				renderPointLabel(dimIdAndPoint.getRight(), context.matrixStack(), context.consumers(), context.camera());
			}
		});

		if (START_POINT_SELECTION.isPressed()) {
			if (setSelection() && selection.dimId.equals(currentDimId)) {
				float[] colorComponents = getSelectionColor();
				outlinePos(selection.getBlockPos(), context, colorComponents[0], colorComponents[1], colorComponents[2], 1f);
			} else return;
		} else if ((SHOW_SELECTION.isPressed() || selectionDisplayLife > 0) && isSelectionValid()) {
			float[] colorComponents = getSelectionColor();
			outlinePos(selection.getBlockPos(), context, colorComponents[0], colorComponents[1], colorComponents[2], 1f);
		}

		if (SEND_SELECTION_TO_EVERYONE.wasPressed()) {
			if (isSelectionValid()) MinecraftClient.getInstance().player.sendChatMessage(getCreatePointMessage());
		} else if (SEND_SELECTION_TO_NEARBY.wasPressed()) {
			if (isSelectionValid()) {
				final BlockPos clientPos = MinecraftClient.getInstance().player.getBlockPos();
				MinecraftClient.getInstance().world.getPlayers().forEach(player -> {
					if (clientPos.isWithinDistance(player.getBlockPos(), 100)) {
						sendSelectionToPlayer(player.getName().getString());
					}
				});
			}
		} else if (SEND_SELECTION_TO_INDIVIDUAL.wasPressed()) {
			if (isSelectionValid()) MinecraftClient.getInstance().setScreen(new PlayerSelectScreen(PLAYER_SELECT_TITLE, new PlayerSelectGui()));
		}
	}

	// based on EntityRenderer#renderLabelIfPresent
	private static void renderPointLabel(BlockPos pos, MatrixStack matrices, VertexConsumerProvider vertexConsumers, Camera camera) {
		matrices.push();
		final Vec3d posCenter = Vec3d.ofCenter(pos);
		final Vec3d cameraPos = MinecraftClient.getInstance().cameraEntity.getPos();
		Vec3d cameraToPos = posCenter.subtract(cameraPos);
		matrices.translate(cameraToPos.x, cameraToPos.y, cameraToPos.z);

		matrices.multiply(camera.getRotation());
		matrices.scale(-0.025f, -0.025f, 0.025f);
		Matrix4f matrix4f = matrices.peek().getModel();
		float backgroundOpacity = MinecraftClient.getInstance().options.getTextBackgroundOpacity(0.25f);
		int backgroundColor = (int)(backgroundOpacity * 255.0f) << 24;

		TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;
		Text text = fillPlaceholder(DISTANCE, "distance", Integer.toString(Math.round(Math.round(posCenter.distanceTo(MinecraftClient.getInstance().player.getPos())))));
		float xOffset = -textRenderer.getWidth(text) / 2;
		textRenderer.draw(text, xOffset, 0, 0x20FFFFFF, false, matrix4f, vertexConsumers, false, backgroundColor, 0xF000F0);

		matrices.pop();
	}

	public static void postRender(WorldRenderContext context) {
		renderingPoint = false;
	}

	public static void sendSelectionToPlayer(String playerName) {
		MinecraftClient.getInstance().player.sendChatMessage("/w " + playerName + " " + getCreatePointMessage());
	}

	@NotNull
	private static String getCreatePointMessage() {
		final BlockPos selectionBlockPos = selection.getBlockPos();
		return CREATED_POINT.getString() + getCreatePointMessageTail(
				Integer.toString(selectionBlockPos.getX()),
				Integer.toString(selectionBlockPos.getY()),
				Integer.toString(selectionBlockPos.getZ()),
				selection.dimId.toString()
		);
	}

	// based on WorldRenderer#drawShapeOutline
	public static void outlinePos(BlockPos pos, WorldRenderContext context, float red, float green, float blue, float alpha) {
		context.profiler().swap("outline");
//		final VertexConsumerProvider consumers = context.consumers();
		final VertexConsumerProvider consumers = ((WorldRendererAccessor)context.worldRenderer()).point_out$getBufferBuilders().getOutlineVertexConsumers();
		if (consumers != null) {
			Vec3d camPos = context.camera().getPos();
			WorldRendererAccessor.point_out$callDrawShapeOutline(
					context.matrixStack(),
					consumers.getBuffer(RenderLayer.getLines()),
					VoxelShapes.fullCube(),
					(double)pos.getX() - camPos.getX(),
					(double)pos.getY() - camPos.getY(),
					(double)pos.getZ() - camPos.getZ(),
					red, green, blue, alpha
			);
		}
	}

	// returns true if selection is not null
	private static boolean setSelection() {
		if (!selectionUpdated) {
			selectionUpdated = true;
			MinecraftClient client = MinecraftClient.getInstance();
			if (client.player == null) {
				selection = null;
				return false;
			}

			@Nullable final HitResult hitResult = client.player.raycast(FeatureControl.getPointSearchDistance(), client.getTickDelta(), false);

//			selection = hitResult == null ? null : new Selection(getCurrentDimId(), hitResult.getPos());
			final Selection newSelection;
			if (hitResult == null) newSelection = null;
			// special case for BlockHitResult because using getPos is unpredictable whether it will be at the targeted
			//   block or offset towards the targeted face by one
			else if (hitResult instanceof BlockHitResult blockHitResult) newSelection = new Selection(getCurrentDimId(), Vec3d.ofCenter(blockHitResult.getBlockPos()));
			else newSelection = new Selection(getCurrentDimId(), hitResult.getPos());

			selection = newSelection;
		}

		return selection != null;
	}

	private static void moveSelection(boolean moveCloser) {
		if (isSelectionValid()) {
			int movementIncrement = getMovementIncrement();
			if (moveCloser) movementIncrement *= -1;

			final BlockPos selectionBlockPos = selection.getBlockPos();
			final Vec3d eyePos = MinecraftClient.getInstance().player.getEyePos();
			if (!selectionBlockPos.equals(new BlockPos(eyePos))) {
				final Vec3d playerToSelectionUnitVec = selection.getPos().subtract(eyePos).normalize();
				selection.add(playerToSelectionUnitVec.multiply(movementIncrement));
			}

			selectionDisplayLife = getPostMovePreviewDuration();
		}
	}

	private static boolean isSelectionValid() {
		if (selection == null) {
			MinecraftClient.getInstance().player.sendMessage(NO_SELECTION, true);
			return false;
		} else if (selection.dimId.equals(getCurrentDimId())) {
			// isChunkLoaded (called by isPosLoaded) always returns true for ClientWorld
//			if (MinecraftClient.getInstance().world.isPosLoaded(selection.getRight().getX(), selection.getRight().getZ())) {
			if (selection.getBlockPos().isWithinDistance(MinecraftClient.getInstance().player.getBlockPos(), 500)) {
				return true;
			} else {
				MinecraftClient.getInstance().player.sendMessage(SELECTION_NOT_LOADED, true);
				return false;
			}
		} else {
			MinecraftClient.getInstance().player.sendMessage(SELECTION_NOT_IN_DIMENSION, true);
			return false;
		}
	}

	private static String removeUsername(String message) {
		return message.replaceFirst("<.*> ", "");
	}
}
